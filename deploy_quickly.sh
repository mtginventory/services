#!/usr/bin/env bash
docker build -t app_deploy .
docker run --rm --entrypoint cat app_deploy app > app
sam build
sam deploy
rm app
docker image prune -f


