package net.mtginventory.api.inventory.converter;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import io.micronaut.core.annotation.Introspected;

import java.time.LocalDateTime;

@Introspected
public class LocalDateTimeConverter implements DynamoDBTypeConverter<String, LocalDateTime> {
    @Override
    public String convert(LocalDateTime object) {
        return object.toString();
    }

    @Override
    public LocalDateTime unconvert(String object) {
        return LocalDateTime.parse(object);
    }
}
