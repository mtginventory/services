package net.mtginventory.api.inventory.service;

import com.amazonaws.auth.AWS4Signer;
import com.amazonaws.internal.config.*;
import com.amazonaws.partitions.model.*;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.std.StdJdkSerializers;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;
import lombok.SneakyThrows;
import net.mtginventory.api.cards.controller.CardController;
import net.mtginventory.api.cards.dto.Card;
import net.mtginventory.api.inventory.dto.CardValue;
import net.mtginventory.api.inventory.dto.InventorySummary;
import net.mtginventory.api.inventory.dto.InventorySummaryRequest;
import net.mtginventory.api.inventory.dto.ModifyCardRequest;
import net.mtginventory.api.inventory.model.Inventory;
import net.mtginventory.api.inventory.model.InventoryCard;
import net.mtginventory.api.inventory.model.UserAudit;
import org.apache.http.client.config.RequestConfig;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

@Singleton
@Introspected(classes = {InventoryService.class, EndpointDiscoveryConfig.class, HostRegexToRegionMapping.class,
        HostRegexToRegionMappingJsonHelper.class, HttpClientConfig.class, HttpClientConfigJsonHelper.class, InternalConfig.class,
        InternalConfigJsonHelper.class, JsonIndex.class, SignerConfig.class, SignerConfigJsonHelper.class, AWS4Signer.class
        , Partitions.class, Partition.class, Region.class, Service.class, Endpoint.class, CredentialScope.class, RequestConfig.class,
        RequestConfig.Builder.class, StdJdkSerializers.AtomicIntegerSerializer.class})
public class InventoryService {

    private final AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
    private final DynamoDBMapper mapper = new DynamoDBMapper(client);
    @Inject
    private ObjectMapper objectMapper;
    @Inject
    private CardController cardController;


    public void save(Inventory inventory) {
        mapper.save(inventory);
        UserAudit audit = UserAudit.builder()
                .inventory(inventory)
                .type("Add Inventory")
                .userId(inventory.getUserId()).build();
        mapper.save(audit);
    }

    public List<Inventory> getInventoryForUser(String userId) {
        DynamoDBQueryExpression<Inventory> query = new DynamoDBQueryExpression<Inventory>()
                .addExpressionAttributeNamesEntry("#userId", "userId")
                .addExpressionAttributeValuesEntry(":userId", new AttributeValue().withS(userId))
                .withKeyConditionExpression("#userId = :userId")
                .withConsistentRead(false)
                .withScanIndexForward(false);
        return mapper.query(Inventory.class, query);
    }

    public boolean inventoryExists(String userId, String inventoryId) {
        return mapper.load(Inventory.class, userId, inventoryId) != null;
    }

    public List<InventoryCard> getCardsForInventory(String inventoryId) {
        DynamoDBQueryExpression<InventoryCard> query = new DynamoDBQueryExpression<InventoryCard>()
                .addExpressionAttributeNamesEntry("#inventoryId", "inventoryId")
                .addExpressionAttributeValuesEntry(":inventoryId", new AttributeValue().withS(inventoryId))
                .withKeyConditionExpression("#inventoryId = :inventoryId")
                .withConsistentRead(false)
                .withScanIndexForward(false);
        return mapper.query(InventoryCard.class, query);
    }

    @SneakyThrows
    public InventoryCard addCardToInventory(ModifyCardRequest modifyCardRequest, String inventoryId, String userId) {
        InventoryCard load = mapper.load(InventoryCard.class, inventoryId, modifyCardRequest.getCardId());
        if (load != null) {
            if (modifyCardRequest.isFoil()) {
                load.setFoilQuantity(load.getFoilQuantity() + modifyCardRequest.getQuantity());
            } else {
                load.setQuantity(load.getQuantity() + modifyCardRequest.getQuantity());
            }
            mapper.save(load);
            auditInventoryCard(modifyCardRequest, inventoryId, userId,"Add Card");
            return load;
        } else {
            Card card = cardController.getCard(modifyCardRequest.getCardId());
            String json = objectMapper.writeValueAsString(card);
            InventoryCard inventoryCard = objectMapper.readValue(json, InventoryCard.class);
            inventoryCard.setCardId(modifyCardRequest.getCardId());
            inventoryCard.setInventoryId(inventoryId);
            if (modifyCardRequest.isFoil()) {
                inventoryCard.setFoilQuantity(modifyCardRequest.getQuantity());
            } else {
                inventoryCard.setQuantity(modifyCardRequest.getQuantity());
            }
            auditInventoryCard(modifyCardRequest, inventoryId, userId, "Add Card");
            mapper.save(inventoryCard);
            return inventoryCard;
        }
    }

    public InventoryCard removeCardFromInventory(ModifyCardRequest modifyCardRequest, String inventoryId, String userId) {
        InventoryCard load = mapper.load(InventoryCard.class, inventoryId, modifyCardRequest.getCardId());
        if (load == null) {
            throw new HttpStatusException(HttpStatus.NOT_FOUND, "Card does not exist in inventory");
        }
        if (modifyCardRequest.isFoil()) {
            if (load.getFoilQuantity() < modifyCardRequest.getQuantity()) {
                throw new HttpStatusException(HttpStatus.BAD_REQUEST, "You can not remove more instances of the card than currently exist in your inventory");
            }
            load.setFoilQuantity(load.getFoilQuantity() - modifyCardRequest.getQuantity());
        } else {
            if (load.getQuantity() < modifyCardRequest.getQuantity()) {
                throw new HttpStatusException(HttpStatus.BAD_REQUEST, "You can not remove more instances of the card than currently exist in your inventory");
            }
            load.setQuantity(load.getQuantity() - modifyCardRequest.getQuantity());
        }
        auditInventoryCard(modifyCardRequest,inventoryId,userId,"Remove Card");
        mapper.save(load);
        return load;
    }

    public void auditInventoryCard(ModifyCardRequest modifyCardRequest, String inventoryId, String userId, String type) {
        UserAudit audit = UserAudit.builder().userId(userId)
                .type(type)
                .modifyCardRequest(modifyCardRequest)
                .inventory(mapper.load(Inventory.class, userId, inventoryId))
                .build();
        mapper.save(audit);
    }

    public InventorySummary getInventorySummary(String inventoryId, InventorySummaryRequest inventorySummaryRequest) {
        InventorySummary inventorySummary = new InventorySummary();
        List<InventoryCard> cardsForInventory = getCardsForInventory(inventoryId);
        addValue(inventorySummary, cardsForInventory, inventorySummaryRequest.getValueCardCount());
        addDistributions(inventorySummary, cardsForInventory);
        return inventorySummary;
    }

    private void addDistributions(InventorySummary inventorySummary, List<InventoryCard> inventoryCards) {
        double maxCardValue = inventoryCards.stream().map(InventoryCard::getPrices)
                .map(p -> Math.max(p.getUsdFoilValue(), p.getUsdValue()))
                .max(Comparator.naturalOrder()).orElse(0d);
        List<Integer> valueBins = new ArrayList<>();
        valueBins.add(0);
        for (int i = 1; i < maxCardValue * 100; i *= 2) {
            valueBins.add(i);
        }
        inventorySummary.setBinnedPriceDistribution(new HashMap<>());
        inventorySummary.setCmcDistribution(new HashMap<>());
        inventorySummary.setColorIdentityDistribution(new HashMap<>());
        inventorySummary.setSetDistribution(new HashMap<>());
        inventorySummary.setTypeDistribution(new HashMap<>());
        inventorySummary.setCreatureTypeDistribution(new HashMap<>());
        inventorySummary.setRarityDistribution(new HashMap<>());
        inventorySummary.setKeywordDistribution(new HashMap<>());
        for (InventoryCard i : inventoryCards) {
            int usdBin = valueBins.stream().filter(v -> v < i.getPrices().getUsdValue() * 100).max(Comparator.naturalOrder()).orElse(0);
            int foilBin = valueBins.stream().filter(v -> v < i.getPrices().getUsdValue() * 100).max(Comparator.naturalOrder()).orElse(0);
            Function<String, AtomicInteger> newAtomicInteger = (s) -> new AtomicInteger();
            inventorySummary.getBinnedPriceDistribution().computeIfAbsent(Integer.toString(usdBin), newAtomicInteger).addAndGet(i.getQuantity());
            inventorySummary.getBinnedPriceDistribution().computeIfAbsent(Integer.toString(foilBin), newAtomicInteger).addAndGet(i.getFoilQuantity());
            int totalQuantity = i.getQuantity() + i.getFoilQuantity();
            inventorySummary.getCmcDistribution().computeIfAbsent(Integer.toString(i.getCmc()), newAtomicInteger).addAndGet(totalQuantity);
            inventorySummary.getSetDistribution().computeIfAbsent(i.getSet(), newAtomicInteger).addAndGet(totalQuantity);
            inventorySummary.getRarityDistribution().computeIfAbsent(i.getRarity(), newAtomicInteger).addAndGet(totalQuantity);
            if (i.getKeywords() != null) {
                for (String keyword : i.getKeywords()) {
                    inventorySummary.getKeywordDistribution().computeIfAbsent(keyword, newAtomicInteger).addAndGet(totalQuantity);
                }
            }
            if (i.getColorIdentity() != null) {
                inventorySummary.getColorIdentityDistribution().computeIfAbsent(i.getColorIdentity().toString(), newAtomicInteger).addAndGet(totalQuantity);
            }
            String type = i.getTypeLine().split("—")[0].trim();
            inventorySummary.getTypeDistribution().computeIfAbsent(type, newAtomicInteger).addAndGet(totalQuantity);
            if (type.equals("Creature")) {
                String creatureType = i.getTypeLine().substring("Creature".length() + 3);
                inventorySummary.getCreatureTypeDistribution().computeIfAbsent(creatureType, newAtomicInteger).addAndGet(totalQuantity);
            }
        }
    }

    private void addValue(InventorySummary inventorySummary, List<InventoryCard> inventoryCards, int count) {
        List<CardValue> sorted = inventoryCards.stream()
                .map(c ->
                        CardValue.builder().cardId(c.getCardId())
                                .cardName(c.getName())
                                .price(c.getPrices().getUsd())
                                .foilPrice(c.getPrices().getUsdFoil())
                                .quantity(c.getQuantity())
                                .foilQuantity(c.getFoilQuantity())
                                .build()).sorted(Comparator.comparing(CardValue::getTotalValue).reversed()).collect(Collectors.toList());
        List<CardValue> valuableCards = sorted.stream()
                .limit(count)
                .collect(Collectors.toList());
        int quantity = sorted.stream().mapToInt(c -> c.getQuantity() + c.getFoilQuantity()).sum();
        double value = sorted.stream().mapToDouble(CardValue::getTotalValue).sum();
        inventorySummary.setCardQuantity(quantity);
        inventorySummary.setValue(value);
        inventorySummary.setValuableCards(valuableCards);
    }

}
