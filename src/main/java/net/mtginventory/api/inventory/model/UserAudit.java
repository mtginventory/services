package net.mtginventory.api.inventory.model;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.mtginventory.api.inventory.dto.ModifyCardRequest;

@Introspected(classes = {UserAudit.class, DynamoDBTypeConvertedJson.Converter.class})
@Data
@DynamoDBTable(tableName = "mtg-user-audit")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserAudit {

    @DynamoDBHashKey(attributeName = "userId")
    private String userId;
    @DynamoDBRangeKey(attributeName = "auditTimestampMs")
    @DynamoDBAutoGeneratedTimestamp(strategy = DynamoDBAutoGenerateStrategy.CREATE)
    private Long auditTimestampMs;
    private String type;
    @DynamoDBTyped(value = DynamoDBMapperFieldModel.DynamoDBAttributeType.M)
    private Inventory inventory;
    @DynamoDBTypeConvertedJson
    private ModifyCardRequest modifyCardRequest;
}
