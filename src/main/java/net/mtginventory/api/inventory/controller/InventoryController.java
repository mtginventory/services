package net.mtginventory.api.inventory.controller;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.*;
import io.micronaut.http.exceptions.HttpStatusException;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.authentication.Authentication;
import io.micronaut.security.rules.SecurityRule;
import net.mtginventory.api.inventory.dto.ModifyCardRequest;
import net.mtginventory.api.inventory.dto.InventorySummary;
import net.mtginventory.api.inventory.dto.InventorySummaryRequest;
import net.mtginventory.api.inventory.model.Inventory;
import net.mtginventory.api.inventory.model.InventoryCard;
import net.mtginventory.api.inventory.service.InventoryService;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Controller("inventory")
@Secured(SecurityRule.IS_AUTHENTICATED)
@Introspected
public class InventoryController {

    @Inject
    private InventoryService inventoryService;

    @Post
    public Inventory createInventory(@Body Inventory inventory, Authentication authentication) {
        inventory.setUserId(authentication.getName());
        inventory.setId(UUID.randomUUID().toString());
        inventoryService.save(inventory);
        return inventory;
    }

    @Get
    public List<Inventory> getInventories(Authentication authentication) {
        return inventoryService.getInventoryForUser(authentication.getName());
    }

    @Get("{inventoryId}")
    @Secured(SecurityRule.IS_ANONYMOUS)
    public List<InventoryCard> getInventoryCard(@PathVariable("inventoryId") String inventoryId) {
            return inventoryService.getCardsForInventory(inventoryId);
    }

    @Get("{inventoryId}/search")
    @Secured(SecurityRule.IS_ANONYMOUS)
    public List<InventoryCard> getInventoryCardSearch(@PathVariable("inventoryId") String inventoryId,@QueryValue("name")String name) {
        String formattedName = name.toLowerCase().trim();
        return inventoryService.getCardsForInventory(inventoryId).stream().filter(i->i.getName().toLowerCase().contains(formattedName)).collect(Collectors.toList());
    }

    @Post("{inventoryId}/add-card")
    public InventoryCard addInventoryCard(Authentication authentication,
                                          @PathVariable("inventoryId") String inventoryId,
                                          @Body @Valid ModifyCardRequest modifyCardRequest){

        if (inventoryService.inventoryExists(authentication.getName(), inventoryId)) {
            return inventoryService.addCardToInventory(modifyCardRequest,inventoryId,authentication.getName());
        }
        throw new HttpStatusException(HttpStatus.BAD_REQUEST,"Inventory does not exist");

    }

    @Post("{inventoryId}/remove-card")
    public InventoryCard removeInventoryCard(Authentication authentication,
                                          @PathVariable("inventoryId") String inventoryId,
                                          @Body @Valid ModifyCardRequest modifyCardRequest){

        if (inventoryService.inventoryExists(authentication.getName(), inventoryId)) {
            return inventoryService.removeCardFromInventory(modifyCardRequest,inventoryId,authentication.getName());
        }
        throw new HttpStatusException(HttpStatus.BAD_REQUEST,"Inventory does not exist");

    }

    @Get("{inventoryId}/summary{?inventorySummaryRequest*}")
    @Secured(SecurityRule.IS_ANONYMOUS)
    public InventorySummary getInventorySummary(@PathVariable("inventoryId")String inventoryId, InventorySummaryRequest inventorySummaryRequest){
        return inventoryService.getInventorySummary(inventoryId,inventorySummaryRequest);
    }

}
