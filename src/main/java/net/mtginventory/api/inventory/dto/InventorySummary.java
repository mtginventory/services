package net.mtginventory.api.inventory.dto;

import io.micronaut.core.annotation.Introspected;
import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Data
@Introspected
public class InventorySummary {
    private double value;
    private int cardQuantity;
    private List<CardValue> valuableCards;
    private Map<String, AtomicInteger> cmcDistribution;
    private Map<String, AtomicInteger> colorIdentityDistribution;
    private Map<String, AtomicInteger> binnedPriceDistribution;
    private Map<String, AtomicInteger> setDistribution;
    private Map<String,AtomicInteger> typeDistribution;
    private Map<String,AtomicInteger> creatureTypeDistribution;
    private Map<String,AtomicInteger> rarityDistribution;
    private Map<String,AtomicInteger> keywordDistribution;

}
