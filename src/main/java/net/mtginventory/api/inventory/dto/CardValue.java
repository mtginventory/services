package net.mtginventory.api.inventory.dto;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Introspected
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CardValue {
    private String cardId;
    private String cardName;
    private int quantity;
    private int foilQuantity;
    private String price;
    private String foilPrice;


    public double getValue() {
        if (price == null) {
            return 0;
        }
        return quantity * Double.parseDouble(price);
    }

    public double getFoilValue() {
        if (foilPrice == null) {
            return 0;
        }
        return foilQuantity * Double.parseDouble(foilPrice);
    }

    public double getTotalValue() {
        return getValue() + getFoilValue();
    }

    public int getTotalQuantity() {
        return quantity + foilQuantity;
    }
}
