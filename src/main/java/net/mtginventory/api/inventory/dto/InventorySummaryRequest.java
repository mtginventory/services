package net.mtginventory.api.inventory.dto;

import io.micronaut.core.annotation.Introspected;
import lombok.Data;


@Data
@Introspected
public class InventorySummaryRequest {
    private int valueCardCount=10;
}
