package net.mtginventory.api.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import edu.umd.cs.findbugs.annotations.NonNull;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.security.authentication.Authentication;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
@Introspected
public class Auth0Authentication implements Authentication {

    private String sub;
    private String nickname;
    private String name;
    private String picture;
    @JsonProperty("updated_at")
    private String updatedAt;


    @NonNull
    @Override
    public Map<String, Object> getAttributes() {
        return new HashMap<>(Map.of(
                "nickname",nickname,
                "name",name,
                "picture",picture
        ));
    }

    @Override
    public String getName() {
        return sub;
    }
}
