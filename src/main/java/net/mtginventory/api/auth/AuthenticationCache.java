package net.mtginventory.api.auth;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConvertedJson;
import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@DynamoDBTable(tableName = "mtg-inventory-auth-cache")
@Introspected
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticationCache {
   @DynamoDBHashKey(attributeName = "token")
   private String token;
   @DynamoDBTypeConvertedJson
   private Auth0Authentication auth0Authentication;
   private long ttl;
}
