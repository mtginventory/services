package net.mtginventory.api.auth;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.authentication.Authentication;
import io.micronaut.security.rules.SecurityRule;

import java.util.Map;

@Controller("user")
@Secured(SecurityRule.IS_AUTHENTICATED)
@Introspected
public class UserController {

    @Get("info")
    public Map<String,Object> getUserInfo(Authentication authentication){
        return authentication.getAttributes();
    }
}
