package net.mtginventory.api.auth;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.security.authentication.Authentication;
import io.micronaut.security.filters.AuthenticationFetcher;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import org.reactivestreams.Publisher;

import javax.inject.Inject;
import java.time.Instant;
import java.util.Optional;

@Introspected()
public class Auth0AuthenticationFetcher implements AuthenticationFetcher {

    @Client("https://dev-oj0ok9xc.us.auth0.com/userinfo")
    @Inject
    private RxHttpClient httpClient;

    private final AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
    private final DynamoDBMapper mapper = new DynamoDBMapper(client);

    @Override
    public Publisher<Authentication> fetchAuthentication(HttpRequest<?> request) {
        Optional<String> token = request.getHeaders().findFirst("Authorization")
                .map(s -> s.substring(s.indexOf(' ')))
                .map(String::trim);
        if (token.isPresent()) {
            Auth0Authentication cachedAuthentication = getCachedAuthentication(token.get());
            if (cachedAuthentication != null) {
                return Flowable.fromArray(cachedAuthentication);
            }
        }
        return Maybe.<String>create(emitter -> {
            token.ifPresent(emitter::onSuccess);
            emitter.onComplete();
        })
                .map(this::getUserInfo)
                .concatMap(r -> httpClient.retrieve(r, Auth0Authentication.class).firstElement())
                .onErrorComplete()
                .doOnSuccess(a -> insertToCache(token.get(), a))
                .cast(Authentication.class)
                .toFlowable();

    }

    private HttpRequest<?> getUserInfo(String token) {
        return HttpRequest.GET("/").bearerAuth(token);
    }

    private void insertToCache(String token, Auth0Authentication authentication) {
        long ttl = 300 + Instant.now().getEpochSecond();
        mapper.save(new AuthenticationCache(token, authentication, ttl));
    }

    private Auth0Authentication getCachedAuthentication(String token){
            AuthenticationCache load = mapper.load(AuthenticationCache.class, token);
            if (load != null && load.getTtl()<Instant.now().getEpochSecond()) {
                return load.getAuth0Authentication();
            }
            return null;
    }
}
