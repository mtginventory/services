package net.mtginventory.api.image;

import com.amazonaws.HttpMethod;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.internal.AWSS3V4Signer;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.authentication.Authentication;
import io.micronaut.security.rules.SecurityRule;

import java.net.URL;
import java.util.Date;

@Controller("image")
@Introspected(classes = {ImageController.class, AWSS3V4Signer.class})
@Secured(SecurityRule.IS_AUTHENTICATED)
public class ImageController {

    private final AmazonS3 client;

    public ImageController() {
        BasicAWSCredentials credentials = new BasicAWSCredentials(System.getenv("ImageUploadUserAccessKey"), System.getenv("ImageUploadUserAccessKeySecret"));
        client = AmazonS3ClientBuilder.standard()
                .withPathStyleAccessEnabled(true)
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .build();
    }

    @Get(value = "auth",produces = "text/plain")
    public String getAuth(@QueryValue("fileName") String fileName, Authentication authentication){
        GeneratePresignedUrlRequest presignedUrlRequest = new GeneratePresignedUrlRequest("mtginventory-content",
                "app/uploaded/"+authentication.getName()+"/"+fileName, HttpMethod.PUT);
        presignedUrlRequest.setExpiration(new Date(System.currentTimeMillis()+30*60*1000));
        URL url = client.generatePresignedUrl(presignedUrlRequest);
        return url.toString();

    }
}
