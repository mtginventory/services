package net.mtginventory.api.cards.controller;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.reactivex.Flowable;
import net.mtginventory.api.cards.dto.Catalog;

import javax.inject.Inject;
import java.util.List;

import static io.micronaut.http.HttpRequest.GET;

@Controller("catalog")
@Secured(SecurityRule.IS_ANONYMOUS)
@Introspected
public class CatalogController {

    private static final List<String> TYPES = List.of("card-names", "artist-names", "word-bank", "creature-types",
            "planeswalker-types", "land-types", "artifact-types", "enchantment-types", "spell-types", "powers", "toughnesses"
            , "loyalties", "watermarks", "keyword-abilities", "keyword-actions", "ability-words");

    @Client(" https://api.scryfall.com/catalog")
    @Inject
    private RxHttpClient httpClient;

    @Get("{type}")
    public Catalog getCatalog(@PathVariable("type") String type) {
        Flowable<Catalog> retrieve = httpClient.retrieve(GET(type), Catalog.class);
        return retrieve.blockingFirst();
    }

    @Get
    public List<String> getTypes(){
        return TYPES;
    }
}
