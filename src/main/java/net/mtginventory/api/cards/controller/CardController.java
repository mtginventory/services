package net.mtginventory.api.cards.controller;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import lombok.extern.slf4j.Slf4j;
import net.mtginventory.api.cards.dto.Card;
import net.mtginventory.api.cards.dto.CardList;
import net.mtginventory.api.cards.dto.Catalog;

import javax.inject.Inject;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static io.micronaut.http.HttpRequest.GET;
import static java.nio.charset.StandardCharsets.UTF_8;

@Controller("card")
@Secured(SecurityRule.IS_ANONYMOUS)
@Introspected
@Slf4j
public class CardController {

    private static final Map<String,Card> CARD_CACHE = new ConcurrentHashMap<>();

    @Client(" https://api.scryfall.com/cards")
    @Inject
    private RxHttpClient httpClient;

    @Get("autocomplete")
    public List<Card> getAutocompleteCards(@QueryValue("q") String queryString) {
        if (queryString.toUpperCase().matches("[A-Z0-9]{3}:.*")) {
            String setId = queryString.substring(0, 3);
            List<String> ids = Arrays.asList(queryString.substring(4).split(","));
            return getCardForSet(setId, ids);
        }
        Catalog cardCatalog = httpClient.retrieve(GET("autocomplete?q=" + URLEncoder.encode(queryString, UTF_8)), Catalog.class).blockingFirst();
        return getCardsFromNames(cardCatalog.getData());
    }

    private List<Card> getCardsFromNames(List<String> names) {
        if (names.isEmpty()) {
            return Collections.emptyList();
        }
        String query = URLEncoder.encode(names.stream().map(n -> "!\"" + n + "\"").collect(Collectors.joining(" or ")), UTF_8);
        String uri = "search?unique=print&q=" + query;
        List<Card> data = httpClient.toBlocking().retrieve(GET(uri), CardList.class).getData();
        data.forEach(c->CARD_CACHE.put(c.getId(),c));
        return data;
    }

    private List<Card> getCardForSet(String setId, List<String> collectorIds) {
        String ids = collectorIds.stream()
                .map(this::flattenId)
                .flatMap(Function.identity())
                .map(String::trim)
                .map(n -> "cn:" + n).collect(Collectors.joining(" or "));
        String query = URLEncoder.encode("e:" + setId + " (" + ids + ")", UTF_8);
        String uri = "search?unique=print&q=" + query;
        log.info(uri);
        try {
            List<Card> data = httpClient.toBlocking().retrieve(GET(uri), CardList.class).getData();
            data.forEach(c->CARD_CACHE.put(c.getId(),c));
            return data;
        }catch (HttpClientResponseException e){
            if (e.getStatus()== HttpStatus.NOT_FOUND) {
                return Collections.emptyList();
            }
            throw e;
        }
    }

    private Stream<String> flattenId(String id){
        if(id.contains("-")){
            String[] split = id.split("-");
            int start = Integer.parseInt(split[0].trim());
            int end = Integer.parseInt(split[1].trim());
            return IntStream.rangeClosed(start, end).mapToObj(Integer::toString);
        }
        return Stream.of(id);
    }

    @Get("{id}")
    public Card getCard(@PathVariable("id") String id) {
        if(CARD_CACHE.containsKey(id))
            return CARD_CACHE.get(id);
        Card retrieve = httpClient.toBlocking().retrieve(GET(id), Card.class);
        CARD_CACHE.put(id,retrieve);
        return retrieve;
    }
}
