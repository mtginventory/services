package net.mtginventory.api.cards.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.core.annotation.Introspected;
import lombok.Data;

@Data
@Introspected
public class CardFace {
    private String object;
    private String name;
    private String manaCost;
    private String typeLine;
    private String oracleText;
    private String artist;
    private String artistId;
    private String illustrationId;

    @JsonProperty("mana_cost")
    public void setmana_cost(String mana_cost) {
        this.manaCost = mana_cost;
    }

    @JsonProperty("type_line")
    public void settype_line(String type_line) {
        this.typeLine = type_line;
    }

    @JsonProperty("oracle_text")
    public void setoracle_text(String oracle_text) {
        this.oracleText = oracle_text;
    }

    @JsonProperty("artist_id")
    public void setartist_id(String artist_id) {
        this.artistId = artist_id;
    }

    @JsonProperty("illustration_id")
    public void setillustration_id(String illustration_id) {
        this.illustrationId = illustration_id;
    }

}
