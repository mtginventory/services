package net.mtginventory.api.cards.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.core.annotation.Introspected;
import lombok.Data;

@Data
@Introspected
public class CardPart {
    private String object;
    private String id;
    private String component;
    private String name;
    private String typeLine;
    private String uri;


    @JsonProperty("type_line")
    public void settype_line(String typeLine) {
        this.typeLine = typeLine;
    }
}
