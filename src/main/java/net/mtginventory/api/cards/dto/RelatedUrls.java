package net.mtginventory.api.cards.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.core.annotation.Introspected;
import lombok.Data;

@Data
@Introspected
public class RelatedUrls {
    private String gatherer;
    private String tcgplayerDecks;
    private String edhrec;
    private String mtgtop8;

    @JsonProperty("tcgplayer_decks")
    public void settcgplayer_decks(String tcgplayerDecks) {
        this.tcgplayerDecks = tcgplayerDecks;
    }
}
