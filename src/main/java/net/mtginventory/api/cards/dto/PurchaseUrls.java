package net.mtginventory.api.cards.dto;

import io.micronaut.core.annotation.Introspected;
import lombok.Data;

@Data
@Introspected
public class PurchaseUrls {
    private String tcgplayer;
    private String cardmarket;
    private String cardhoarder;
}
