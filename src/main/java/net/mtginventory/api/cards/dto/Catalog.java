package net.mtginventory.api.cards.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.core.annotation.Introspected;
import lombok.Data;

import java.util.List;

@Introspected
@Data
public class Catalog {
    private String object;
    private String uri;
    private int totalValues;
    private List<String> data;

    @JsonProperty("total_values")
    public void settotal_values(int totalValues) {
        this.totalValues = totalValues;
    }
}
