package net.mtginventory.api.cards.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.core.annotation.Introspected;
import lombok.Data;

@Data
@Introspected
public class Prices {
    private String usd;
    private String usdFoil;
    private String eur;
    private String tix;


    @JsonProperty("usd_foil")
    public void setusd_foil(String usd_foil){
        this.usdFoil=usd_foil;
    }

    @JsonIgnore
    public double getUsdValue(){
        if(usd==null)
            return 0;
        return Double.parseDouble(usd);
    }

    @JsonIgnore
    public double getUsdFoilValue(){
        if(usdFoil==null)
            return 0;
        return Double.parseDouble(usdFoil);
    }

    @JsonIgnore
    public double getEurValue(){
        if(eur==null)
            return 0;
        return Double.parseDouble(eur);
    }

    @JsonIgnore
    public double getTixValue(){
        if(tix==null)
            return 0;
        return Double.parseDouble(tix);
    }
}
