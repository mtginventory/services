package net.mtginventory.api.cards.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.core.annotation.Introspected;
import lombok.Data;

@Data
@Introspected
public class ImageUris {
    private String small;
    private String normal;
    private String large;
    private String png;
    private String artCrop;
    private String borderCrop;


    @JsonProperty("art_crop")
    public void setart_crop(String art_crop) {
        this.artCrop = art_crop;
    }

    @JsonProperty("border_crop")
    public void setborder_crop(String border_crop) {
        this.artCrop = border_crop;
    }

}
