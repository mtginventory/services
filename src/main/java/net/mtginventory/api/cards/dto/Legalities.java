package net.mtginventory.api.cards.dto;

import io.micronaut.core.annotation.Introspected;
import lombok.Data;

@Data
@Introspected
public class Legalities {
    private String standard;
    private String future;
    private String historic;
    private String pioneer;
    private String moder;
    private String legacy;
    private String pauper;
    private String vintage;
    private String penny;
    private String commander;
    private String brawl;
    private String duel;
    private String oldschool;
}
