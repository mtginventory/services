package net.mtginventory.api.cards.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.core.annotation.Introspected;
import lombok.Data;

import java.util.List;

@Data
@Introspected
public class CardList {
    private String object;
    private int totalCards;
    private boolean hasMore;
    private List<Card> data;

    @JsonProperty("total_cards")
    public void settotal_card(int totalCards){
        this.totalCards=totalCards;
    }
    @JsonProperty("has_more")
    public void sethas_more(boolean hasMore){
        this.hasMore=hasMore;
    }
}
