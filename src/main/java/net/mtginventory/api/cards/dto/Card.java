package net.mtginventory.api.cards.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.core.annotation.Introspected;
import lombok.Data;

import java.util.List;

@Introspected
@Data
public class Card {
    private String object;
    private String id;
    private String oracleId;
    private List<Integer> multiverseIds;
    private String name;
    private String lang;
    private String releasedAt;
    private String uri;
    private String scryfallUri;
    private String layout;
    private boolean highresImage;
    private ImageUris imageUris;
    private String manaCost;
    private int cmc;
    private String typeLine;
    private String oracleText;
    private String power;
    private String toughness;
    private List<String> colors;
    private List<String> colorIdentity;
    private List<String> keywords;
    private Legalities legalities;
    private List<String> games;
    private boolean reserved;
    private boolean foil;
    private boolean nonfoil;
    private boolean oversised;
    private boolean promo;
    private boolean reprint;
    private boolean variation;
    private String set;
    private String setName;
    private String setType;
    private String setUri;
    private String setSearchUri;
    private String scryfallSetUri;
    private String rulingsUri;
    private String printsSearchUri;
    private String collectorNumber;
    private boolean digital;
    private String rarity;
    private String flavorText;
    private String cardBackId;
    private String artist;
    private List<String> artistIds;
    private String illustrationId;
    private String borderColor;
    private String frame;
    private List<String> frameEffects;
    private boolean fullArt;
    private boolean textless;
    private boolean booster;
    private boolean storySpotlight;
    private int edhrecRank;
    private List<CardPart> allParts;
    private List<CardFace> cardFaces;
    private Prices prices;

    @JsonProperty("oracle_id")
    private void setoracle_id(String oracleId) {
        this.oracleId = oracleId;
    }

    @JsonProperty("multiverse_ids")
    public void setmultiverse_ids(List<Integer> multiverse_ids) {
        this.multiverseIds = multiverse_ids;
    }

    @JsonProperty("released_at")
    public void setreleased_at(String released_at) {
        this.releasedAt = released_at;
    }

    @JsonProperty("scryfall_uri")
    public void setscryfall_uri(String scryfall_uri) {
        this.scryfallUri = scryfall_uri;
    }

    @JsonProperty("highres_image")
    public void sethighres_image(boolean highres_image) {
        this.highresImage = highres_image;
    }

    @JsonProperty("image_uris")
    public void setimage_uris(ImageUris image_uris) {
        this.imageUris = image_uris;
    }

    @JsonProperty("mana_cost")
    public void setmana_cost(String mana_cost) {
        this.manaCost = mana_cost;
    }

    @JsonProperty("type_line")
    public void settype_line(String type_line) {
        this.typeLine = type_line;
    }

    @JsonProperty("oracle_text")
    public void setoracle_text(String oracle_text) {
        this.oracleText = oracle_text;
    }

    @JsonProperty("color_identity")
    public void setcolor_identity(List<String> color_identity) {
        this.colorIdentity = color_identity;
    }

    @JsonProperty("card_faces")
    public void setcard_faces(List<CardFace> card_faces) {
        this.cardFaces = card_faces;
    }

    @JsonProperty("story_spotlight")
    public void setstory_spotlight(boolean story_spotlight) {
        this.storySpotlight = story_spotlight;
    }

    @JsonProperty("edhrec_rank")
    public void setedhrec_rank(int edhrec_rank) {
        this.edhrecRank = edhrec_rank;
    }

    @JsonProperty("all_parts")
    public void setall_parts(List<CardPart> all_parts) {
        this.allParts = all_parts;
    }

    @JsonProperty("flavor_text")
    public void setflavor_text(String flavor_text) {
        this.flavorText = flavor_text;
    }

    @JsonProperty("card_back_id")
    public void setcard_back_id(String card_back_id) {
        this.cardBackId = card_back_id;
    }

    @JsonProperty("artist_ids")
    public void setartist_ids(List<String> artist_ids) {
        this.artistIds = artist_ids;
    }

    @JsonProperty("illustration_id")
    public void setillustration_id(String illustration_id) {
        this.illustrationId = illustration_id;
    }

    @JsonProperty("border_color")
    public void setborder_color(String border_color) {
        this.borderColor = border_color;
    }

    @JsonProperty("frame_effects")
    public void setframe_effects(List<String> frame_effects) {
        this.frameEffects = frame_effects;
    }

    @JsonProperty("full_art")
    public void setfull_art(boolean full_art) {
        this.fullArt = full_art;
    }

    @JsonProperty("set_type")
    public void setset_type(String set_type) {
        this.setType = set_type;
    }

    @JsonProperty("set_name")
    public void setset_name(String set_name) {
        this.setName = set_name;
    }

    @JsonProperty("set_uri")
    public void setset_uri(String set_uri) {
        this.setUri = set_uri;
    }

    @JsonProperty("set_search_uri")
    public void setset_search_uri(String set_search_uri) {
        this.setSearchUri = set_search_uri;
    }

    @JsonProperty("scryfall_set_uri")
    public void setscryfall_set_uri(String scryfall_set_uri) {
        this.scryfallSetUri = scryfall_set_uri;
    }

    @JsonProperty("rulings_uri")
    public void setrulings_uri(String rulings_uri) {
        this.rulingsUri = rulings_uri;
    }

    @JsonProperty("prints_search_uri")
    public void setprints_search_uri(String prints_search_uri) {
        this.printsSearchUri = prints_search_uri;
    }

    @JsonProperty("collector_number")
    public void setcollector_number(String collector_number) {
        this.collectorNumber = collector_number;
    }


}
