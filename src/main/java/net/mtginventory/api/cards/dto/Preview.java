package net.mtginventory.api.cards.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.core.annotation.Introspected;
import lombok.Data;

@Data
@Introspected
public class Preview {
    private String source;
    private String sourceUri;
    private String previewedAt;

    @JsonProperty("source_uri")
    public void setsource_uri(String sourceUri) {
        this.sourceUri = sourceUri;
    }

    @JsonProperty("previewed_at")
    public void setpreviewd_at(String previewedAt) {
        this.previewedAt = previewedAt;
    }
}
